﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace translateAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class translateController : ControllerBase
    {
        static readonly HttpClient client = new HttpClient();

        [HttpGet]
        public async Task<IActionResult> Get(String from = "", String to = "", String phrase = "")
        {
            
            //Console.WriteLine(phrase.GetType());
            if (from == "" || from == null || to == "" || to == null || phrase == "" || phrase == null)
            {
                Console.WriteLine("Missing parmeters");
                return BadRequest(new { msg ="Missing parmeters" });
            }
            try
            {
                HttpResponseMessage response = await client.GetAsync(String.Format("http://node-red-api:1880/api/?from={0}&to={1}&phrase={2}", from, to, phrase));
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                JObject json = JObject.Parse(responseBody);
                Console.WriteLine(responseBody);
                return Ok(responseBody);
            }
            catch
            {
                Console.WriteLine("StatusCode(500)");
                return StatusCode(500);
            }

  
        }
    }
}
